#include <gtk/gtk.h>
#include <libintl.h>
#include <locale.h>

static void print_hello( 
    GtkWidget *widget,
    gpointer  data
    ) { 
    g_print( gettext("Hello World") );
    g_print( "\n" );
    }

static void activate ( 
    GtkApplication  *app,
    gpointer        user_data
    ) { 
    // Variable declarations
    GtkWidget *window;
    GtkWidget *button;

    window = gtk_application_window_new( app );
    gtk_window_set_title( GTK_WINDOW(window), "Window" );
    gtk_window_set_default_size( GTK_WINDOW(window), 200, 200 );

    button = gtk_button_new_with_label( gettext("Hello World") );
    g_signal_connect( button, "clicked", G_CALLBACK(print_hello), NULL );

    gtk_container_add( GTK_CONTAINER(window), button );

    gtk_widget_show_all( window );
    
    }
 
void intl_init() { 
    setlocale( LC_ALL, "" );
    bindtextdomain( "testapp", "locale" );
    textdomain( "testapp" );
    }

int main (
    int argc,
    char **argv
    ) {
    // Variable declarations
    GtkApplication *app; // the application
    int status; // the application's return status

    intl_init();

    app = gtk_application_new( "de.nobodyinperson.testapp", 
        G_APPLICATION_FLAGS_NONE );
    g_signal_connect( app, "activate", G_CALLBACK(activate), NULL );
    status = g_application_run( G_APPLICATION(app), argc, argv );
    g_object_unref( app );

    return status;
    }
